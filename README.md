# Tracker container images

This repository is obsolete.

It was previously used to build container images suitable for building and
testing the [Tracker](https://gitlab.gnome.org/GNOME/tracker) and [Tracker
Miners](https://gitlab.gnome.org/GNOME/tracker-miners) projects, as well
as [libmediaart](https://gitlab.gnome.org/GNOME/libmediaart/).

All three projects have now migrated to ci-templates:
[libmediaart](https://gitlab.gnome.org/GNOME/libmediaart/-/merge_requests/13/pipelines),
[tracker-miners](https://gitlab.gnome.org/GNOME/tracker-miners/-/merge_requests/347)
and
[tracker](https://gitlab.gnome.org/GNOME/tracker-miners/-/merge_requests/347).
